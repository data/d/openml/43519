# OpenML dataset: Brazilian-IBOV-Historical-Data-from-1992-to-2019

https://www.openml.org/d/43519

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

About the dataset
In this dataset you can find prices data from the biggest brazillian stock index(IBOV) from 1992 to 2019 and also day of the week and month informations:

date: date in format dd/mm/yy
dayofweek: day of week as string
month: the month of the year
open: open prices in points
close: close prices in points
high: high prices in points
low: low prices in points

Inspiration
How does the IBOV index behave over the years? 
What is the average annual return?
In what month does this index tend to perform better?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43519) of an [OpenML dataset](https://www.openml.org/d/43519). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43519/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43519/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43519/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

